module wisepay/request_edi

go 1.13

require (
	github.com/Rhymond/go-money v0.4.0
	github.com/jinzhu/gorm v1.9.11
)
