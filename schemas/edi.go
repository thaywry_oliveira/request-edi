package schemas

type InfoEdi struct {
	Detalhes   []InfoDetalhes
	Pagination InfoPagination
}

type InfoDetalhes struct {
	Parcela          string
	TransactionCode  string  `json:"codigo_transacao"`
	TaxaAntecipacao  float64 `json:"taxa_antecipacao"`
	DataMovimentacao string  `json:"data_movimentacao"`
}

type InfoPagination struct {
	Elements      int
	TotalPages    int
	Page          int
	TotalElements int
}
