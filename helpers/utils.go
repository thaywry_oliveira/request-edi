package helpers

import (
	"fmt"
	"time"
)

func FloatToCents(input float64) int64 {
	val := input * 100
	return int64(val)
}

func FormatNotificationCode(code string) string {
	part1 := code[0:8]
	part2 := code[8:12]
	part3 := code[12:16]
	part4 := code[16:20]
	part5 := code[20:32]
	return fmt.Sprintf("%v-%v-%v-%v-%v", part1, part2, part3, part4, part5)
}

func FormatarMes(then time.Time) string {
	var mes string
	if int(then.Month()) < 10 {
		mes = fmt.Sprintf("0%v", int(then.Month()))
	} else {
		mes = fmt.Sprintf("%v", int(then.Month()))
	}

	return mes
}

func FormatarDia(then time.Time) string {
	var dia string
	if then.Day() < 10 {
		dia = fmt.Sprintf("0%v", int(then.Day()))
	} else {
		dia = fmt.Sprintf("%v", int(then.Day()))
	}

	return dia
}
