package use_cases

import (
	"fmt"
	"wisepay/request_edi/helpers"
	"wisepay/request_edi/models"
	"wisepay/request_edi/schemas"

	"github.com/jinzhu/gorm"
)

func UpdateAntecipacao(db *gorm.DB, infoEdi []schemas.InfoDetalhes) {
	for _, edi := range infoEdi {
		var recebimentoParcela models.RecebimentoParcela

		recebimentoID := getRecebimentoID(db, edi.TransactionCode)
		db.Where("vl_custo_antecipacao_gp_centavos = ? AND recebimento_id = ? AND numero = ?", 0, recebimentoID, edi.Parcela).First(&recebimentoParcela)
		if recebimentoParcela.Id > 0 {
			recebimentoParcela.CustoAntecipacaoGpCentavos = helpers.FloatToCents(edi.TaxaAntecipacao)
			recebimentoParcela.DataAntecipacaoAdquirente = edi.DataMovimentacao
			db.Save(&recebimentoParcela)
			fmt.Printf("Id .: %v Atualizado!\n", recebimentoParcela.Id)
		}
		// db.Model(&recebimentoParcela).Where("recebimento_id = ?", recebimentoID).Updates(models.RecebimentoParcela{
		// 	CustoAntecipacaoGpCentavos: 4.74,
		// })
		// fmt.Println(recebimentoParcela.Id)
		// fmt.Println(recebimentoParcela)
		// fmt.Println(recebimentoID)
		// fmt.Println(edi)
	}

}

func getRecebimentoID(db *gorm.DB, transactionCode string) uint {
	transactionCodeFormated := helpers.FormatNotificationCode(transactionCode)
	var recebimento models.Recebimento

	db.Where("transaction_code = ?", transactionCodeFormated).First(&recebimento)
	return recebimento.Id
}
