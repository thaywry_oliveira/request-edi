package models

type Recebimento struct {
	Id              uint
	OperacaoId      uint   `gorm:"column:operacao_id"`
	TransactionCode string `gorm:"column:transaction_code"`
}

func (Recebimento) TableName() string {
	return "recebimentos"
}
