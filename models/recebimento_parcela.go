package models

type RecebimentoParcela struct {
	Id                         uint
	CustoAntecipacaoGpCentavos int64   `gorm:"column:vl_custo_antecipacao_gp_centavos"`
	TaxaAntecipacao            float64 `gorm:"column:taxa_antecipacao"`
	DataAntecipacaoAdquirente  string  `gorm:"column:data_antecipacao_adquirente"`
	// UpdatedAt                  time.Time
}

func (RecebimentoParcela) TableName() string {
	return "recebimento_parcelas"
}
