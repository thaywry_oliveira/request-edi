package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"wisepay/request_edi/helpers"
	"wisepay/request_edi/schemas"
	useCases "wisepay/request_edi/use_cases"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	clientId = "101671511"
	token    = "201c735af6024a909765692b41fae8be"
	pageSize = 100 // Quantos resultados por Página na API

	driver   = "postgres"
	host     = "localhost"
	port     = 5432
	user     = ""
	dbname   = ""
	password = 0
	sslmode  = "disable"
)

func main() {
	db := connectDB()
	defer db.Close()

	// data := time.Date(2018, 9, 06, 0, 0, 0, 0, time.UTC)
	// result := requestApi(data, 1, clientId, token, pageSize)
	// if result.Detalhes != nil {
	// 	useCases.UpdateAntecipacao(db, result.Detalhes)
	// }

	for i := 0; i <= 1080; i++ {
		data := time.Date(2017, 8, 01, 0, 0, 0, 0, time.UTC).AddDate(0, 0, i)
		result := requestApi(data, 1, clientId, token, pageSize)
		if result.Detalhes != nil {
			totalPages := result.Pagination.TotalPages
			useCases.UpdateAntecipacao(db, result.Detalhes)
			if totalPages > 1 {
				newResult := requestPagination(totalPages, data, clientId, token, pageSize)
				useCases.UpdateAntecipacao(db, newResult.Detalhes)
			}
		} else {
			panic("Erro na API")
		}
	}

}

func connectDB() *gorm.DB {
	conn := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=%v", host, port, user, dbname, password, sslmode)
	db, err := gorm.Open(driver, conn)
	if err != nil {
		fmt.Println(err)
		panic("failed to connect database")
	}
	db.Exec("SET SEARCH_PATH to paymentx")
	db.LogMode(true)
	return db
}

func requestPagination(totalPages int, data time.Time, clientId string, token string, pageSize int) schemas.InfoEdi {
	var result schemas.InfoEdi
	for t := 2; t <= totalPages; t++ {
		result = requestApi(data, t, clientId, token, pageSize)
	}
	return result
}

func requestApi(data time.Time, page int, clientId string, token string, pageSize int) schemas.InfoEdi {
	mes := helpers.FormatarMes(data)
	dia := helpers.FormatarDia(data)

	dataMovimento := fmt.Sprintf("%v-%v-%v", data.Year(), mes, dia)
	fmt.Printf("Data Movimento .: %v \n", dataMovimento)
	fmt.Printf("Pagina .: %v \n", page)
	pageNumber := page

	url := fmt.Sprintf(
		"https://api.r2tec.com/edi/v1/2.00/movimentos?dataMovimento=%v&pageNumber=%v&pageSize=%v&tipoMovimento=3",
		dataMovimento, pageNumber, pageSize,
	)

	req, err := http.NewRequest("GET", url, nil)
	req.SetBasicAuth(clientId, token)

	if err != nil {
		fmt.Println("Errei na Url")
		log.Fatalln(err)
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Errei no CLient DO")
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Errei no Body")
		log.Fatalln(err)
	}

	result := string(body)
	infoEdi := schemas.InfoEdi{}
	json.Unmarshal([]byte(result), &infoEdi)
	return infoEdi
}
